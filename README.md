# About
Сервис для хранения файлов на Node.js, Express, Postrgres, Sequelize и Docker.
Сделан Голенко Вадимом для проектной деятельности Мосполитеха.

# Server
http://media-service.udachin.tech

# Documentation API

#### Загрузить файл на сервер

URI: `POST /file/upload`

request body: 
```
FormData:
    file: <твой_файл>
```

response body: 
```
JSON:
{
    data: {
        id: 1, // number
        fileUrl: 'http://media-service.udachin.tech/1592675249182.png', // string
        name: '1592675249182.png' // string
    },
    error: false, // boolean
    errorText: null, // string | null
    additionalErrors: [], // string[]
}
```

#### Удалить файл с сервера

URI: `DELETE /file/:fileId`
<br>
Example request: `DELETE http://media-service.udachin.tech/file/1`
 
request body: 
```
none
```

response body: 
```
JSON:
{
    data: null, // null
    error: false, // boolean
    errorText: null, // string | null
    additionalErrors: [], // string[]
}
```
