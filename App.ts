const express = require('express');
const http = require('http');
import sequelize from './src/lib/db' ;
import { seedDB } from './src/lib/db_seed' ;
import middlewares from './src/middlewares';
import routes from './src/routes';
const app = express();

app.use(express.static('uploads'));

middlewares.forEach((middleware: any) => app.use(middleware));
routes.forEach((el: any) => app.use(el.path, el.route));

const start = async () => {
    try {
        const server = http.createServer(app);

        await sequelize.authenticate().catch((e) => {
            console.error(e);
            process.exit(0);
        });

        await sequelize.sync(
                { force: Boolean(process.env.DEV) }
            );
        console.log(`Sequelize is ready to work with "${process.env.DB_NAME}" database...`);
        await seedDB();
        await server.listen(process.env.PORT);
        console.log(`Server is now running on port ${process.env.PORT} ...`)
    } catch (e) {
        console.log(e)
    }
}

start();
