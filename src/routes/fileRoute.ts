// file
import express from 'express';
import { requestCreateFile, requestDeleteFile } from "../requests/file";
const router = express.Router();

router.post('/upload', requestCreateFile);
router.delete('/:fileId', requestDeleteFile);

export default router;
