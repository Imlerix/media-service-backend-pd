import { FileModel } from '../../models'
import ErrnoException = NodeJS.ErrnoException;
const fs = require('fs');

type ParamsType = {
  id: number
};

export const deleteFile = async ({ id }: ParamsType) => {

  let isError = false;

  const file = await FileModel.findOne({
    where: {
      id
    }
  });

  if (!file) {
    return Promise.reject({
      message: 'Файл не найден.'
    })
  }

  fs.unlink(`uploads/${file.name}`, (err: ErrnoException | null) => {
    if (err) {
      isError = true;
    }
  });

  if (isError) {
    return Promise.reject({
      message: 'Произошла ошибка при удалении файла.'
    })
  }

  return FileModel.destroy({
    where: {
      id
    }
  });
};
