import { FileModel } from '../../models'
import { UploadedFile } from "express-fileupload";

export const createFile = async (file: UploadedFile) => {

  const regexp = /\.[A-Z]*$/gi;

  const extension = file.name.match(regexp);

  const fileName = `${Date.now()}${extension}`;

  await file.mv(`./uploads/${fileName}`);

  return FileModel.create({
    name: fileName
  }, { fields: [ 'id', 'name' ] });
};
