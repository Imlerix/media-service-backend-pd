import { AllowNull, AutoIncrement, Column, DataType, Model, PrimaryKey, Table } from "sequelize-typescript";
import sequelize from "../lib/db";

@Table({
    modelName: 'file'
})
export class File extends Model<File> {
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.INTEGER)
    id: number;

    @AllowNull(false)
    @Column(DataType.TEXT)
    name: string;
}

sequelize.addModels([File]);
