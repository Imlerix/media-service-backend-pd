import { createFile } from "../../controllers/file";
import { Request, Response, NextFunction } from 'express';
import { answer200 } from "../_constants";

const reqParams = [
  'file'
];

export const requestCreateFile = async(req: Request, res: Response, next: NextFunction) => {
  try{
    reqParams.forEach(item => {
      if (!req.files[item]) {
        throw new Error('Не передан файл');
      }
    });

    // @ts-ignore
    const file = await createFile(req.files[reqParams[0]]);

    res.status(200).json(answer200({
      data: {
        id: file.id,
        name: file.name,
        fileUrl: `http://${req.headers.host}/${file.name}`,
      }
    }));

  }catch (e) {
    res.status(500).json(answer200({error: true, errorText: e.message}))
  }
  next()
};
