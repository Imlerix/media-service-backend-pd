import { deleteFile } from "../../controllers/file";
import { Request, Response, NextFunction } from 'express'
import { answer200 } from "../_constants";

export const requestDeleteFile = async(req: Request, res: Response, next: NextFunction) => {
  try{
    if (!req.params.fileId) {
      throw new Error('Не передан id файла');
    }

    await deleteFile({ id: Number(req.params.fileId) });

    res.status(200).json(answer200({}));

  }catch (e) {
    res.status(500).json(answer200({error: true, errorText: e.message}))
  }
  next()
};
